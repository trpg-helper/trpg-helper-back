import json

import flask_jwt_extended
import pytest
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jti)
from tinydb import where

from trpg_helper.db import get_db, close_db


def test_register(client, app):
    response = client.post(
        '/auth/register',
        data=json.dumps({'username': 'a', 'password': 'b'}),
        content_type='application/json'
    )
    assert response.is_json
    assert 'access_token' in response.get_json()

    with app.app_context():
        assert get_db().table('users').get(where('username') == 'a') is not None


def test_auth_register(auth):
    response = auth.register('a', 'b')
    assert response.is_json
    assert 'access_token' in response.get_json()


def test_auth_already_register(auth):
    response = auth.register('Dilbert', 'b')
    assert response.status_code == 400
    assert response.is_json
    assert 'User with username %s already registered' % 'Dilbert' \
        == response.get_json()['msg']


def test_empty_register(client):
    response = client.post(
        '/auth/register',
        data={}
    )
    assert response.status_code == 400
    assert response.is_json
    assert "errors" in response.get_json()


def test_wrong_register(client):
    response = client.post(
        '/auth/register',
        data=json.dumps({'user': 'Dilbert'}),
        content_type='application/json'
    )
    assert response.status_code == 400
    assert response.is_json
    assert "errors" in response.get_json()


def test_login(client, app):
    response = client.post(
        '/auth/login',
        data=json.dumps({'username': 'Dilbert', 'password': '12345678'}),
        content_type='application/json'
    )
    assert response.status_code == 200
    assert response.is_json
    assert 'access_token' in response.get_json()
    assert 'refresh_token' in response.get_json()


def test_empty_login(client):
    response = client.post(
        '/auth/login',
        data={}
    )
    assert response.status_code == 400
    assert response.is_json
    assert "errors" in response.get_json()


def test_wrong_login(client):
    response = client.post(
        '/auth/login',
        data=json.dumps({'user': 'Dilbert'}),
        content_type='application/json'
    )
    assert response.status_code == 400
    assert response.is_json
    assert "errors" in response.get_json()


def test_unknown_login(auth):
    response = auth.login('a', 'b')
    assert response.status_code == 400
    assert response.is_json
    assert 'User with username %s does not exists' % 'a' == response.get_json()[
        'msg']


def test_incorrect_password_login(auth):
    response = auth.login('Dilbert', '1234')
    assert response.status_code == 401
    assert response.is_json
    assert 'Incorrect password for user %s' % 'Dilbert' == response.get_json()[
        'msg']


def test_auth_login(auth):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()


def test_logout(client, auth, app):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()

    jwt = response.get_json()['access_token']
    response = client.post(
        '/auth/logout',
        headers={
            'Authorization': f'Bearer {jwt}'
        }
    )
    print(response.get_json())
    assert response.status_code == 200
    assert response.is_json
    assert 'Access token has been revoked' == response.get_json()['msg']

    token = get_jti(jwt)
    with app.app_context():
        assert get_db().table('revoked_tokens').get(where('token') == token) is not None


def test_auth_logout(client, auth, app):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()
    assert 'refresh_token' in response.get_json()

    jwt = response.get_json()['access_token']
    response = auth.logout(jwt)
    assert response.status_code == 200
    assert response.is_json
    assert 'Access token has been revoked' == response.get_json()['msg']

    token = get_jti(jwt)
    with app.app_context():
        assert get_db().table('revoked_tokens').get(where('token') == token) is not None


def test_empty_logout(client, auth, app):
    response = auth.logout('')
    assert response.status_code == 422


def test_logout_with_refresh_token(auth):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()
    assert 'refresh_token' in response.get_json()

    refresh_token = response.get_json()['refresh_token']
    response = auth.logout(refresh_token)
    assert response.status_code == 422


def test_logout_with_revoked_access_token(auth):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()

    access_token = response.get_json()['access_token']
    response = auth.logout(access_token)
    assert response.status_code == 200

    response = auth.logout(access_token)
    assert response.status_code == 401


def test_unknown_user_logout(auth):
    dummy_access_token = create_access_token(identity='a')

    response = auth.logout(dummy_access_token)
    assert response.status_code == 400
    assert response.is_json
    assert "msg" in response.get_json()
    assert 'User with username %s does not exists' % 'a' == response.get_json()[
        'msg']


def test_refresh(client, auth):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()
    assert 'refresh_token' in response.get_json()

    token = response.get_json()['access_token']
    refresh_token = response.get_json()['refresh_token']

    response = client.post(
        '/auth/refresh',
        headers={
            'Authorization': f'Bearer {refresh_token}'
        }
    )
    assert response.status_code == 200
    assert response.is_json
    assert 'access_token' in response.get_json()

    new_token = response.get_json()['access_token']
    assert new_token != refresh_token

    response = auth.logout(token)
    assert response.status_code == 200

    response = auth.logout(new_token)
    assert response.status_code == 200


def test_auth_refresh(client, auth, app):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()
    assert 'refresh_token' in response.get_json()

    access_token = response.get_json()['access_token']
    refresh_token = response.get_json()['refresh_token']

    response = auth.refresh(refresh_token)
    assert response.status_code == 200
    assert response.is_json
    assert 'access_token' in response.get_json()

    new_token = response.get_json()['access_token']
    assert new_token != refresh_token

    response = auth.logout(access_token)
    assert response.status_code == 200

    response = auth.logout(new_token)
    assert response.status_code == 200


def test_unknown_user_refresh(auth):
    dummy_refresh_token = create_refresh_token(identity='a')

    response = auth.refresh(dummy_refresh_token)
    assert response.status_code == 401
    assert response.is_json
    assert "msg" in response.get_json()
    assert 'User with username %s does not exists' % 'a' == response.get_json()[
        'msg']
