import json

import flask_jwt_extended
import pytest
from tinydb import where

from trpg_helper.sockets import socketio


def test_connect_without_login(app):

    with pytest.raises(flask_jwt_extended.exceptions.NoAuthorizationError) as e:
        socketio.test_client(app)

    assert "Missing Authorization Header" in str(e)


def test_connect(auth, app):
    response = auth.login()
    access_token = response.get_json()['access_token']

    socket = socketio.test_client(app,
                                  headers={
                                      'Authorization': f'Bearer {access_token}'
                                  })
    assert socket is not None

    socket.disconnect()


def test_init_connection_when_logout(auth, app):
    response = auth.login()
    assert response.is_json
    access_token = response.get_json()['access_token']

    auth.logout(access_token)

    with pytest.raises(flask_jwt_extended.exceptions.RevokedTokenError) as e:
        socketio.test_client(app,
                             headers={
                                 'Authorization': f'Bearer {access_token}'
                             })


def test_revoked_token_after_logout(auth, app):
    response = auth.login()
    assert response.is_json
    access_token = response.get_json()['access_token']

    socket = socketio.test_client(app,
                                  headers={
                                      'Authorization': f'Bearer {access_token}'
                                  })
    assert socket is not None

    auth.logout(access_token)

    with pytest.raises(flask_jwt_extended.exceptions.RevokedTokenError) as e:
        socket.connect(headers={
            'Authorization': f'Bearer {access_token}'
        })


def test_connect_after_refresh(auth, app):
    response = auth.login()
    assert response.is_json
    access_token = response.get_json()['access_token']
    refresh_token = response.get_json()['refresh_token']

    socket = socketio.test_client(app,
                                  headers={
                                      'Authorization': f'Bearer {access_token}'
                                  })
    assert socket is not None

    response = auth.refresh(refresh_token)
    assert response.is_json
    assert 'access_token' in response.get_json()
    new_access_token = response.get_json()['access_token']

    socket.connect(headers={
        'Authorization': f'Bearer {new_access_token}'
    })
