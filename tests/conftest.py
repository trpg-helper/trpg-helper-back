import json
import os
import tempfile

import pytest
from flask_dotenv import DotEnv
from werkzeug.security import generate_password_hash

from trpg_helper import create_app
from trpg_helper.db import close_db, get_db, init_db
from trpg_helper.sockets import socketio


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp(suffix='.json')

    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
        'SECRET_KEY': 'sth',
        'PRESERVE_CONTEXT_ON_EXCEPTION': False,
    })

    with app.app_context():
        init_db()
        db = get_db()
        users = db.table('users')
        users.insert({'username': 'Dilbert', 'password': generate_password_hash('12345678')})

    yield app

    with app.app_context():
        close_db()

    os.close(db_fd)
    # os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='Dilbert', password='12345678'):
        return self._client.post(
            '/auth/login',
            data=json.dumps({
                'username': username,
                'password': password
                }),
            content_type='application/json'
        )

    def register(self, username, password):
        return self._client.post(
            '/auth/register',
            data=json.dumps({
                'username': username,
                'password': password
                }),
            content_type='application/json'
        )

    def refresh(self, token):
        return self._client.post(
            '/auth/refresh',
            headers={
                'Authorization': f'Bearer {token}'
            }
        )

    def logout(self, token):
        return self._client.post(
            '/auth/logout',
            headers={
                'Authorization': f'Bearer {token}'
            }
        )


@pytest.fixture
def auth(client):
    return AuthActions(client)
