import pytest

def test_token(client, auth):
    response = auth.login()
    assert response.is_json
    assert 'access_token' in response.get_json()

    token = response.get_json()['access_token']
    response = client.get(
        '/test/token',
        headers={
            'Authorization': f'Bearer {token}'
        }
    )
    assert response.status_code == 200
    assert response.is_json
    assert 'message' in response.get_json()
    assert 'Todo OK' == response.get_json()['message']