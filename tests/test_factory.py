from flask import g

from trpg_helper import create_app
from trpg_helper.db import close_db

def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_db_close(app):
    with app.app_context():
        close_db()
        assert g.get('db') is None
