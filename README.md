TRPG Helper
===

TRPG Helper is a web application which allow rpg players to have an easiest management of sheets and resources of a role playing game.

Dependencies
---

- Python>=3.6
- Pipenv>=2018.7.1

Install environment
---

Install Pipfile dependencies:
```
$ pipenv install
```

Optional, install Pipfile development dependencies:
```
$ pipenv install --dev
```

Configure server
---

Edit `.env` file with follow template configuration:
```
FLASK_APP=run.py
FLASK_ENV=<production|development>
SECRET_KEY=<secret_key>
DATABASE=<JSON database path>
SERVER_NAME=<your_ip>:<port>
```

Run tests
---

To run test suite you need to install development dependencies.

Run backend test suite:
```
$ pipenv run tests 
```

Retrieve coverage report:
```
$ pipenv run cov
```

Or run [_coverage_][1] tool directly:
```
$ pipenv run coverage <options>
```

Run server
---

Run server into pipenv environment:
```
$ pipenv run python run.py
```

Or run with pipenv scripts:
```
$ pipenv run server # or
$ pipenv run debug
```

Documentation
---

TRPG Helper documentation can be found at [doc folder](./doc/README.md). Also, when the server is running index route server a swagger documentation of specs of the backend, this documentation includes namespaces, endpoints, authentication specification and models used by the application.


[1]: https://coverage.readthedocs.io/en/coverage-4.5.1a/
