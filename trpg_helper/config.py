from flask_dotenv import DotEnv

env = DotEnv()

def init_config(app):
	env.init_app(app)
