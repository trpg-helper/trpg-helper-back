import click
from flask import current_app, g
from flask.cli import with_appcontext
from tinydb import TinyDB, where
from tinydb.database import Table


def get_db():
    if 'db' not in g:
        DATABASE = current_app.config.get('DATABASE') or 'database/db.json'
        g.db = db = TinyDB(DATABASE)

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    db = get_db()


def reset_db():
    db = get_db()
    db.purge_tables()
    db.purge()


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


@click.command('reset-db')
@with_appcontext
def reset_db_command():
    """Clear the existing data and create new tables."""
    reset_db()
    click.echo('Reset the database.')
