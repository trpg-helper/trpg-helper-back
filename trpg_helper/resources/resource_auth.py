from flask import request
from flask_jwt_extended import (get_jwt_identity, get_raw_jwt,
                                jwt_refresh_token_required, jwt_required)
from flask_restplus import Namespace, Resource, Model, fields

from ..auth import login_user, logout_user, refresh_user_token, register_user

auth_ns = Namespace('auth')
user_login_model = auth_ns.model('UserLogin', {
    'username': fields.String(required=True),
    'password': fields.String(required=True)
})
access_token_model = auth_ns.model('AccessToken', {
    'access_token': fields.String
})
accessnrefresh_token_model = auth_ns.inherit('AccessNRefreshTokens', access_token_model, {
    'refresh_token': fields.String
})

@auth_ns.route('/login')
@auth_ns.response(200, 'OK: User loged in')
@auth_ns.response(400, 'Error: Invalid password')
@auth_ns.response(401, 'Error: Unknown user')
@auth_ns.response(500, 'Server error')
class Login(Resource):

    @auth_ns.expect(user_login_model, validate=True)
    def post(self):

        try:
            params = request.get_json()
            username = params.get('username', None)
            password = params.get('password', None)
            access_token, refresh_token = login_user(username, password)
            ret = {
                'access_token': access_token,
                'refresh_token': refresh_token
            }
            return ret, 200
        except IndexError as ex:
            return {"msg": str(ex)}, 400
        except ValueError as ex:
            return {"msg": str(ex)}, 401
        except Exception as ex:
            return {"msg":  "Server error"}, 500


@auth_ns.route('/logout')
@auth_ns.doc(security='bearerAuth')
@auth_ns.response(200, 'OK: User loged out and access token is revoked')
@auth_ns.response(400, 'Error: Incorrect username')
@auth_ns.response(401, 'Error: Revoked or invalid token')
@auth_ns.response(422, 'Error: Unprocessable token. Perhaps using refresh token?')
@auth_ns.response(500, 'Error: Server error')
class Logout(Resource):

    @jwt_required
    def post(self):
        try:
            token = get_raw_jwt()['jti']
            username = get_jwt_identity()
            logout_user(username, token)
            return {"msg": "Access token has been revoked"}, 200
        except IndexError as ex:
            return {"msg": str(ex)}, 400
        except Exception as ex:
            return {"msg": "Server error"}, 500


@auth_ns.route('/refresh')
@auth_ns.doc(security='bearerAuth')
@auth_ns.param('jwt', description="JWT refresh token", _in="query", required=True)
@auth_ns.response(200, 'OK: Access token refreshed')
@auth_ns.response(401, 'Error: Invalid token')
@auth_ns.response(422, 'Error: Unprocessable token. Perhaps using access token?')
@auth_ns.response(500, 'Error: Server error')
class Refresh(Resource):

    @jwt_refresh_token_required
    def post(self):
        try:
            current_user = get_jwt_identity()
            access_token = refresh_user_token(current_user)
            ret = {
                'access_token': access_token,
            }
            return ret, 200
        except IndexError as ex:
            return {"msg": str(ex)}, 401
        except Exception as ex:
            return {"msg":  "Server error"}, 500


@auth_ns.route('/register')
@auth_ns.response(200, 'OK')
@auth_ns.response(400, 'Error: Incorrect password')
@auth_ns.response(401, 'Error: Unknown user')
@auth_ns.response(500, 'Server error')
class Register(Resource):

    @auth_ns.expect(user_login_model, validate=True)
    def post(self):

        params = request.get_json()
        username = params.get('username', None)
        password = params.get('password', None)

        try:
            access_token, refresh_token = register_user(username, password)
            ret = {
                'access_token': access_token,
                'refresh_token': refresh_token
            }
            return ret, 200
        except IndexError as ex:
            return {"msg": str(ex)}, 400
        except Exception as ex:
            return {"msg": "Server error"}, 500


def init_app(api):
    api.add_namespace(auth_ns)
