from flask_restplus import Api
from . import resource_auth, resource_test
from ..auth import jwt


authorizations = {
    'bearerAuth': {
        'type': 'http',
        'scheme': 'bearer',
        'bearerFormat': 'JWT'
    }
}
api = Api(authorizations=authorizations)
jwt._set_error_handler_callbacks(api)


def init_app(app):

    resource_auth.init_app(api)
    if app.debug:
        resource_test.init_app(api)

    api.init_app(app)
