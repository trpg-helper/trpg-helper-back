from flask_jwt_extended import jwt_required, get_current_user
from flask_restplus import Namespace, Resource

test_ns = Namespace('test')


@test_ns.route('/token')
@test_ns.doc(security='bearerAuth')
class TestToken(Resource):

    @jwt_required
    def get(self):
        return {'message': 'Todo OK'}, 200


def init_app(api):
    api.add_namespace(test_ns)
