import json

from flask_cors import CORS

from . import resources
from .auth import (get_jwt_identity, login_user, logout_user,
                   refresh_user_token, register_user)

cors = CORS()


def init_urls(app):
    resources.init_app(app)
    cors.init_app(app)
