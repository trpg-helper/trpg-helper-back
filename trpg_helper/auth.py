from datetime import timedelta

from flask import jsonify
from flask_jwt_extended import (JWTManager, create_access_token,
                                create_refresh_token, get_jwt_identity)
from werkzeug.security import generate_password_hash, check_password_hash

from .db import create_user, get_user, revoke_token, is_revoked_token


jwt = JWTManager()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return is_revoked_token(jti)


def login_user(username, password):
    user = get_user(username)

    if not user:
        raise IndexError('User with username %s does not exists' % username)

    if not check_password_hash(user['password'], password):
        raise ValueError('Incorrect password for user %s' % username)

    access_token = create_access_token(identity=username)
    refresh_token = create_refresh_token(identity=username)

    return access_token, refresh_token


def register_user(username, password):
    create_user(username, generate_password_hash(password))
    return login_user(username, password)


def refresh_user_token(username):
    user = get_user(username)
    if not user:
        raise IndexError('User with username %s does not exists' % username)

    return create_access_token(identity=username)


def logout_user(username, token):
    user = get_user(username)
    if not user:
        raise IndexError('User with username %s does not exists' % username)

    revoke_token(token)


def init_auth(app):

    app.config['JWT_TOKEN_LOCATION'] = ['headers']
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(seconds=1)
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(days=1)
    jwt.init_app(app)
