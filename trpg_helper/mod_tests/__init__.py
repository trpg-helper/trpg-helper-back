from .controllers import test_ns

def init_api(api):
    api.add_namespace(test_ns)
