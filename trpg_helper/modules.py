import json

from flask_cors import CORS
from flask_restplus import Api

from . import mod_auth, mod_tests

cors = CORS()
authorizations = {
    'bearerAuth': {
        'type': 'http',
        'scheme': 'bearer',
        'bearerFormat': 'JWT'
    }
}
api = Api(authorizations=authorizations)


def init_modules(app):
    mod_auth.init_app(app)
    mod_auth.init_api(api)
    mod_tests.init_api(api)
    api.init_app(app)
    cors.init_app(app)
