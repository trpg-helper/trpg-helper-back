import os

from eventlet import monkey_patch
monkey_patch()

from flask import Flask, url_for, jsonify
from flask_jwt_extended import jwt_required, get_raw_jwt


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    from .config import init_config
    init_config(app)

    if test_config:
        # load the test config if passed in
        app.config.update(**test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .urls import init_urls
    init_urls(app)

    from .sockets import init_sockets
    init_sockets(app)

    from .auth import init_auth
    init_auth(app)

    return app
