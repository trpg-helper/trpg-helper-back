from datetime import timedelta

from .controllers import auth_ns
from .auth import jwt


def init_app(app):

    app.config['JWT_TOKEN_LOCATION'] = ['headers']
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(seconds=1)
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(days=1)

    jwt.init_app(app)

def init_api(api):
    jwt._set_error_handler_callbacks(api)
    api.add_namespace(auth_ns)
