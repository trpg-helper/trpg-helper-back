from tinydb import where

from ..db import get_db

def create_user(username, password):
    db = get_db()
    users = db.table('users')

    if get_user(username):
        raise IndexError('User with username %s already registered' % username)

    users.insert({
        'username': username,
        'password': password
    })


def get_user(username):
    db = get_db()
    users = db.table('users')
    return users.get(where('username') == username)


def revoke_token(token):
    db = get_db()
    revoked_tokens = db.table('revoked_tokens')
    revoked_tokens.insert({'token': token})


def is_revoked_token(token):
    db = get_db()
    revoked_tokens = db.table('revoked_tokens')
    return revoked_tokens.contains(where('token') == token)