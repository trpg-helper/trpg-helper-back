import os

from eventlet import monkey_patch
from flask import Flask

from . import *

monkey_patch()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    from .config import init_config
    init_config(app)

    if test_config:
        # load the test config if passed in
        app.config.update(**test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .modules import init_modules
    init_modules(app)

    from .sockets import init_sockets
    init_sockets(app)

    return app
