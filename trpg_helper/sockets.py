from flask import g
from flask_jwt_extended import get_jwt_identity, get_raw_jwt, jwt_required
from flask_socketio import SocketIO, emit

from .mod_auth.auth import logout_user

async_mode = 'eventlet'
socketio = SocketIO(async_mode=async_mode)

@socketio.on('connect')
@jwt_required
def connect():
    print('Connection')

@socketio.on('disconnect')
@jwt_required
def disconnect():
    print('Disconnection')

def init_sockets(app):
    socketio.init_app(app)
