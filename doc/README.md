TRPG Helper
===

TRPG Helper is a web application which allow rpg players to have an easiest management of sheets and resources of a role playing game.

This web application will help game masters to:
- [ ] Manage games' rooms
- [ ] Allow users to join to games
- [ ] Create templates of player sheets with different types of attributes

This web application will help game players to:
- [ ] Join and leave games' rooms
- [ ] Fill and update attributes' sheets of a game
- [ ] Preserve player sheets between connections


Other documentation
---

- [UML Diagrams](./uml/README.md)
